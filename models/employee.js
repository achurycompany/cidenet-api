import mongoose, { Schema } from "mongoose";
import mongoosePaginate from "mongoose-paginate-v2";

const employeeSchema = new Schema({
  firstName: {
    type: String,
    required: true,
    uppercase: true
  },
  otherName: {
    type: String,
    uppercase: true
  },
  middleName: {
    type: String,
    required: true,
    uppercase: true
  },
  lastName: {
    type: String,
    uppercase: true
  },
  country: {
    type: String,
    required: true
  },
  typeDni: {
    type: String,
    required: true
  },
  numberDni: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  admissionDate: {
    type: Date
  },
  departmentCompany: {
    type: String
  },
  state: {
    type: Boolean,
    default: true
  },
  updateAt: {
    type: Date
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
});

employeeSchema.index({ name: 1 });

employeeSchema.plugin(mongoosePaginate);

const Employee = mongoose.model("employee", employeeSchema);

export default Employee;
