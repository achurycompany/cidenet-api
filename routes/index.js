import routerx from "express-promise-router";
import employeeRouter from "./employee";

const router = routerx();

router.use("/employee", employeeRouter);

export default router;
