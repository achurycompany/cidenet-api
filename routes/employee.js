import routerx from "express-promise-router";
import employeeController from "../controller/employeeController.js";

const router = routerx();

router.post("/add", employeeController.add);
router.post("/generate", employeeController.generateEmail);
router.put("/edit", employeeController.update);
router.get("/search", employeeController.query);
router.put("/delete", employeeController.delete);
router.get("/list", employeeController.list);

export default router;
