require("dotenv").config();

const conexion = {
  host: process.env.MONGODB_HOST,
  port: process.env.MONGODB_PORT,
  database: process.env.MONGODB_DATABASE,
  user: process.env.MONGODB_USER,
  pass: process.env.MONGODB_PASS
};

module.exports = {
  // Base de datos de producción
  mongoURI: `mongodb://${conexion.user}:${conexion.pass}@${conexion.host}:${conexion.port}/${conexion.database}`
};
