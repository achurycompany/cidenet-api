if (process.env.NODE_ENV === "production") {
  console.log("Corriendo en modo PRODUCCION!");
  module.exports = require("./prod");
} else {
  console.log("Corriendo en modo DESARROLLO!");
  module.exports = require("./dev");
}
