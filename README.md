# cidenet-API

## Notas para la configuracion

Para iniciar el proyecto en modo desarrollo se recomienda instalar de manera global
https://www.npmjs.com/package/nodemon

## Configuracion de .env para iniciar la base de datos MONGODB

se deja un ejemplo que puede encontrar en el archivo .env.example
una vez configurado crear la base de datos en #mongodb

```
MONGODB_HOST=localhost
MONGODB_PORT=27017
MONGODB_DATABASE=cidenet-db

MONGODB_USER=user
MONGODB_PASS=12345
```

# Configurar whitelist para conexiones CORS

dentro del archivo index.js encontrara la siguiente linea

```
 whitelist = ["http://localhost:8080", "http://localhost:8081"];
```

debe configurar los puertos que esuchara, por defecto estan estos pero puede cambiarlos a su gusto.

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles for production

```
npm run production
```

```
npm run start
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
