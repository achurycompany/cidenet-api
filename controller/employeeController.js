import models from "../models/";
const selfCrud = {
  add: async (req, res, next) => {
    try {
      let objdata = req.body;
      objdata.updateAt = new Date();
      console.log(objdata);
      let queryID = await models.Employee.findOne({
        typeDni: objdata.typeDni,
        numberDni: objdata.numberDni
      });
      console.log("id", queryID);
      if (!queryID) {
        const reg = await models.Employee.create(objdata);
        res.status(200).json({ data: reg, status: 200, message: "Created" });
      } else {
        res.json({ data: "email", status: 422, message: "Error number ID" });
      }
    } catch (error) {
      res.status(500).send({
        message: "Error de registro"
      });
      next(error);
    }
  },
  generateEmail: async (req, res, next) => {
    try {
      let objdata = req.body;
      let idNumber = Math.floor(Math.random() * 10) + 1;
      let format_name = objdata.firstName;
      let format_middleName = objdata.middleName.replace(/\s/g, "");
      let email_country =
        objdata.country === "Colombia" ? "cidenet.com.co" : "cidenet.com.us";
      let email_ref = `${format_name}.${format_middleName}@${email_country}`;
      let resp_email = email_ref.toUpperCase();
      let reg = await models.Employee.findOne({
        email: resp_email
      });
      if (!reg) {
        res
          .status(200)
          .json({ data: resp_email, status: 200, message: "Created" });
      } else {
        while (resp_email === reg.email) {
          email_ref = `${format_name}.${format_middleName}${idNumber}@${email_country}`;
          resp_email = email_ref.toUpperCase();
        }
        res
          .status(200)
          .json({ data: resp_email, status: 200, message: "Created" });
      }
    } catch (error) {
      res.status(500).send({
        message: "Error de registro"
      });
      next(error);
    }
  },
  update: async (req, res, next) => {
    try {
      let objdata = req.body;
      objdata.updateAt = new Date();
      objdata.state = true;
      let queryID = await models.Employee.findOne({
        typeDni: objdata.typeDni,
        numberDni: objdata.numberDni
      });
      if (!queryID) {
        const reg = await models.Employee.findByIdAndUpdate(
          {
            _id: req.body._id
          },
          objdata,
          {
            new: true
          }
        );
        res.status(200).json({ data: reg, status: 200, message: "Created" });
      } else {
        if (String(queryID._id) === String(objdata._id)) {
          const reg = await models.Employee.findByIdAndUpdate(
            {
              _id: req.body._id
            },
            objdata,
            {
              new: true
            }
          );
          res.status(200).json({ data: reg, status: 200, message: "Created" });
        } else {
          res.json({
            data: "numberDni",
            status: 422,
            message: "Error number o type ID"
          });
        }
      }
    } catch (error) {
      res.status(500).send({
        message: "Error en la actualizacion"
      });
      next(error);
    }
  },
  query: async (req, res, next) => {
    try {
    } catch (error) {
      res.status(500).send({
        message: "Error de consulta"
      });
      next(error);
    }
  },
  list: async (req, res, next) => {
    try {
      let page = req.query.page || 1;
      let search = req.query.search || "";
      let filter = req.query.filter || "";
      let custom = req.query.custom || "";
      console.log("valor de search", search);
      console.log("valor de filter", filter);
      console.log("valor de filter", custom);
      let queryFind = { state: true };
      // search all
      if (filter == "all") {
        queryFind = {
          $and: [{ state: true }],
          $or: [
            { middleName: { $regex: new RegExp(search, "i") } },
            { lastName: { $regex: new RegExp(search, "i") } },
            { firstName: { $regex: new RegExp(search, "i") } },
            { otherName: { $regex: new RegExp(search, "i") } },
            { country: { $regex: new RegExp(search, "i") } },
            { typeDni: { $regex: new RegExp(search, "i") } },
            { numberDni: { $regex: new RegExp(search, "i") } },
            { departmentCompany: { $regex: new RegExp(search, "i") } },
            { email: { $regex: new RegExp(search, "i") } }
          ]
        };
      }
      if (filter == "middleName") {
        queryFind = {
          $and: [{ state: true }],
          $or: [{ middleName: { $regex: new RegExp(search, "i") } }]
        };
      }
      if (filter == "lastName") {
        queryFind = {
          $and: [{ state: true }],
          $or: [{ lastName: { $regex: new RegExp(search, "i") } }]
        };
      }
      if (filter == "firstName") {
        queryFind = {
          $and: [{ state: true }],
          $or: [{ firstName: { $regex: new RegExp(search, "i") } }]
        };
      }
      if (filter == "otherName") {
        queryFind = {
          $and: [{ state: true }],
          $or: [{ otherName: { $regex: new RegExp(search, "i") } }]
        };
      }
      if (filter == "email") {
        queryFind = {
          $and: [{ state: true }],
          $or: [{ email: { $regex: new RegExp(search, "i") } }]
        };
      }
      if (filter == "numberDni") {
        queryFind = {
          $and: [{ state: true }],
          $or: [{ numberDni: { $regex: new RegExp(search, "i") } }]
        };
      }
      if (filter == "typeDni") {
        queryFind = {
          $and: [{ state: true }, { typeDni: custom }],
          $or: [{ numberDni: { $regex: new RegExp(search, "i") } }]
        };
      }
      if (filter == "country") {
        queryFind = {
          $and: [{ state: true }, { country: custom }],
          $or: [
            { middleName: { $regex: new RegExp(search, "i") } },
            { lastName: { $regex: new RegExp(search, "i") } },
            { firstName: { $regex: new RegExp(search, "i") } },
            { otherName: { $regex: new RegExp(search, "i") } },
            { country: { $regex: new RegExp(search, "i") } },
            { typeDni: { $regex: new RegExp(search, "i") } },
            { numberDni: { $regex: new RegExp(search, "i") } },
            { departmentCompany: { $regex: new RegExp(search, "i") } },
            { email: { $regex: new RegExp(search, "i") } }
          ]
        };
      }
      let options = {
        page,
        sort: { createdAt: -1 }
      };
      let reg = await models.Employee.paginate(queryFind, options);
      if (!reg) {
        res.status(404).send({
          message: "el registro no existe"
        });
      } else {
        console.log(reg);
        res.status(200).json(reg);
      }
    } catch (error) {
      res.status(500).send({
        message: "Error de consulta"
      });
      next(error);
    }
  },
  delete: async (req, res, next) => {
    try {
      console.log(req.body);
      let action = req.body.action;
      if (action == true) {
        const reg = await models.Employee.findByIdAndDelete({
          _id: req.body._id
        });
        res.status(200).json({ data: reg, status: 200, message: "eliminado" });
      } else {
        res.json({
          data: "action",
          status: 422,
          message: "Error action"
        });
      }
    } catch (error) {
      res.status(500).send({
        message: "Error de consulta"
      });
      next(error);
    }
  }
};
export default selfCrud;
