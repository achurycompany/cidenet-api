import express from "express";
import http from "http";
import path from "path";
import mongoose from "mongoose";
import bodyParser from "body-parser";
import morgan from "morgan";
import cookieSession from "cookie-session";
import keys from "./config/keys";
import cors from "cors";
//routes import
import router from "./routes/";
mongoose.Promise = global.Promise;
mongoose
  .connect(keys.mongoURI, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    poolSize: 15,
    serverSelectionTimeoutMS: 95000, // Keep trying to send operations for 5 seconds
    socketTimeoutMS: 95000, // Close sockets after 45 seconds of inactivity
    keepAlive: true, // For long running applications
    keepAliveInitialDelay: 900000 // For long running applications,
  })
  .then(() =>
    console.log("DB Connected!", `MongoDB connected at ${keys.mongoURI}`)
  )
  .catch((err) => {
    console.log("Error - DB Not Connected!!:", keys.mongoURI, err.message);
  });
//Get the default connection
const app = express();
app.use(morgan("combined"));
// define the whitelisted domains and set the CORS options to check them
var whitelist = [];
if (process.env.NODE_ENV !== "production") {
  whitelist = ["http://localhost:8080", "http://localhost:8081"];
} else {
  whitelist = ["http://localhost:8080", "http://localhost:8081"];
}
var corsOptions = {
  origin: function (origin, callback) {
    var originWhitelisted = whitelist.indexOf(origin) !== -1;
    callback(null, originWhitelisted);
  }
};
app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
// making ./public as the static directory
app.use(express.static(path.join(__dirname, "public")));
app.use(
  cookieSession({
    maxAge: 30 * 24 * 60 * 60 * 1000,
    keys: [keys.cookieKey]
  })
);
// auth ruta
app.use("/api", router);
const PORT = process.env.PORT || 5004;
const server = http.createServer(app);
server.listen(PORT);
console.log("Sever listening on: ", PORT);
